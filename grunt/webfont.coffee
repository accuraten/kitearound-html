module.exports =
  icons:
    src: 'src/svg-icons/*.svg'
    dest: 'public/fonts'
    types: 'eot,woff,ttf'
    options:
      font: 'ka-icons'
      relativeFontPath: '../fonts'
      syntax: 'bootstrap'
      stylesheet: 'less'
      templateOptions:
        baseClass: 'ka-icon'
        classPrefix: 'ka-'
        mixinPrefix: 'ka-'