# Replacement for deprecated $.toggle()
$.fn.clicktoggle = (a, b) ->
	@each ->
		clicked = false
		$(this).click ->
			if clicked
				clicked = false
				return b.apply(this, arguments)
			clicked = true
			a.apply this, arguments

		return

$(document).ready ->
	# Function for animating rotation in HTML elements
	$.fn.animateRotate = (startAngle, endAngle, duration, easing, complete) ->
		@each ->
			elem = $(this)
			$(deg: startAngle).animate
				deg: endAngle
			,
				duration: duration
				easing: easing
				step: (now) ->
					elem.css
						"-moz-transform": "rotate(" + now + "deg)"
						"-webkit-transform": "rotate(" + now + "deg)"
						"-o-transform": "rotate(" + now + "deg)"
						"-ms-transform": "rotate(" + now + "deg)"
						transform: "rotate(" + now + "deg)"

					return

				complete: complete or $.noop

			return

	# Scroll window to top
	$("a#button-to-top").click (event) ->
		$("html,body").animate
			scrollTop: 0
		, 300
		return

	# Toggling menu with button

	# Init of Bootstrap component
	$("#menu").collapse({toggle: false})

	$(".menu-button a").clicktoggle ->
		$("#menu").collapse('show')
		$(".menu-button i").animateRotate(0, -90, 300, "swing")
		return
	, ->
		$("#menu").collapse('hide')
		$(".menu-button i").animateRotate(-90, 0, 300, "swing")
		return

	# Collapsing containers after H2 in mobile versions
	# 1. Find all H2 in content area
	# 2. Select only those, that contain wiki content in the middle
	# 3. Make wrapper for such content
	# 4. Make this wrapper collapsible with H2 button
	enquire.register "(max-width: 640px)",
		match: ->
			# Looking for H2s, to wrap them
			$(".wiki-content h2").each (index) ->
				if $(this).nextAll("h2").length > 0
					# If after our H2 is another H2
					$(this).nextUntil("h2").wrapAll("<div class='collapsible-h2-wrapper' />")
				else
					# If not, than search until the end of parent
					$(this).nextAll().wrapAll("<div class='collapsible-h2-wrapper' />")

			$(".collapsible-h2-wrapper").each (index) ->
				$(this).addClass("collapse-media")
				$(this).prev("h2").addClass("collapsed")
				$(this).prev("h2").children().wrapAll("<a href='#uncollapse' class='collapsible-h2-wrapper-a' />")
			$(".collapsible-h2-wrapper-a").click ->
				$(event.target).closest("h2").toggleClass("collapsed").nextAll(".collapsible-h2-wrapper:lt(1)").toggleClass("collapse-media")
				return
		unmatch: ->
			$('a.collapsible-h2-wrapper-a').contents().unwrap()
			$("h2.collapsed").removeClass("collapsed")
			$(".collapsible-h2-wrapper").removeClass("collapse-media").contents().unwrap()

	# JSON with markers

	map_markers = [
		{
			lat: 10.916746485747343
			lng: 108.2947011522217
			name: "15kn"
			type: "weather-station"
			href: "spot.html"
		}
		{
			lat: 10.940848856476558
			lng: 108.19454473876954
			name: "Willy Willy"
			type: "school"
			href: "school.html"
		}
		{
			lat: 10.94691627753898
			lng: 108.20003790283204
			name: "Africa"
			type: "shop"
			href: "shop.html"
		}
		{
			lat: 10.952815039985326
			lng: 108.21325582885743
			name: "Ozone"
			type: "shop"
			href: "shop.html"
		}
	]

	# Default Google Maps style

	ka_styles = [
		{
			featureType: "administrative"
			elementType: "all"
			stylers: [color: "#00d3ff"]
		}
		{
			featureType: "administrative"
			elementType: "geometry.stroke"
			stylers: [color: "#00bee6"]
		}
		{
			featureType: "administrative"
			elementType: "labels.text.fill"
			stylers: [color: "#005466"]
		}
		{
			featureType: "administrative"
			elementType: "labels.text.stroke"
			stylers: [color: "#00bee6"]
		}

		{
			featureType: "landscape"
			elementType: "all"
			stylers: [color: "#00a9cc"]
		}
		{
			featureType: "poi"
			elementType: "all"
			stylers: [visibility: "off"]
		}
		{
			featureType: "road"
			elementType: "all"
			stylers: [
				{color: "#0095b3"}
			]
		}
		{
			featureType: "road"
			elementType: "labels"
			stylers: [visibility: "off"]
		}
		{
			featureType: "transit"
			elementType: "all"
			stylers: [
				{visibility: "simplified"}
				{color: "#0095b3"}
			]
		}
		{
			featureType: "water"
			elementType: "all"
			stylers: [
				{
					color: "#0088a3"
				}
				{
					visibility: "on"
				}
			]
		}
	]

	$("#spot-location-map").each (index) ->
		lat = parseFloat($(this).attr("data-lat"))
		lng = parseFloat($(this).attr("data-lng"))

		# If we pass scrollwheel "true", make it true
		# Otherwise its false by default
		scrollwheel = $(this).attr("data-scrollwheel") == 'true'
		showmarkers = $(this).attr("data-showmarkers") == 'true'

		# True by default
		marker_display = $(this).attr("data-marker-display") != 'false'

		if !$(this).attr("data-zoom")?
			zoom = 10
		else
			zoom = parseInt($(this).attr("data-zoom"))

		initialize = ->
		mapOptions =
			center:
				lat: lat
				lng: lng
			styles: ka_styles
			panControl: false
			zoomControl: true
			scaleControl: true
			scrollwheel: scrollwheel
			streetViewControl: false

			mapTypeId: google.maps.MapTypeId.TERRAIN

			mapTypeControlOptions:
				position: google.maps.ControlPosition.RIGHT_BOTTOM

			zoomControlOptions:
				style: google.maps.ZoomControlStyle.LARGE
				position: google.maps.ControlPosition.LEFT_BOTTOM

			zoom: zoom

		id = $(this).attr("id")

		map = new google.maps.Map(document.getElementById(id), mapOptions)

		map.setOptions

		if !showmarkers
			if marker_display
				myLatLng = new google.maps.LatLng(mapOptions.center.lat, mapOptions.center.lng)

				marker = new MarkerWithLabel(
					position: myLatLng
					icon: " "
					map: map
					labelContent: "<span class='fa-stack fa-2x'><i class='fa fa-circle fa-stack-2x marker-school'></i><i class='fa fa-map-marker fa-stack-1x fa-inverse'></i></span>"
					labelAnchor: new google.maps.Point(20, 20)
					labelClass: "map-marker-label2" # the CSS class for the label
				)
				marker.setMap map
		else
			jQuery.each map_markers, (i, item) ->
				myLatLng = new google.maps.LatLng(item.lat, item.lng)

				css_marker_style = "marker-" + item.type

				marker = new MarkerWithLabel(
					position: myLatLng
					icon: " "
					map: map
					labelContent: "<div class='map-marker-container'><a class='map-marker' href='" + item.href + "'><div class='map-marker-icon'><span class='fa-stack fa-2x'><i class='fa fa-circle fa-stack-2x " + css_marker_style + "'></i><i class='fa fa-map-marker fa-stack-1x fa-inverse'></i></span></div><div class='map-marker-label'>" + item.name + "</div></a></div>"
					labelAnchor: new google.maps.Point(20, 10)
					labelClass: "map-marker-label2" # the CSS class for the label
				)

				google.maps.event.addListener marker, "click", ->
					window.location.href = item.href

				marker.setMap map

		return
		google.maps.event.addDomListener window, "load", initialize

	# Fixed sticky initialize
	$('.share-widget').fixedsticky()

	# Carousel initialize
	$('.carousel').carousel()

	# Selectpicker initialize
	$('.selectpicker').selectpicker
		style: 'btn-select'

	$('.selectpicker-table').selectpicker
		style: 'btn-select-table'

	# Enabling mobile select
	if /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)
		$('.selectpicker, .selectpicker-table').selectpicker 'mobile'




