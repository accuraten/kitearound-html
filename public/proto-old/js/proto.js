$(document).ready(function() {
	// AJAX simulation for guide-login-and-registration.html
	var bgs = ["i/guide-progress-indicator-01.png", "i/guide-progress-indicator-02.png", "i/guide-progress-indicator-03.png", "i/guide-progress-indicator-04.png"];
	var bg_index = 0;
	$("#progress-indicator").click(function () { 
	    bg_index++;
		if (bg_index == 4) {
			bg_index = 0;
		}
		var bg_img_src = bgs[bg_index];
      	$(this).attr("src", bg_img_src);
    });
	// AJAX simulation for shop-directory.html
	$("#shop-directory").each(function(index) {
		var bgs = ["url(i/shop-directory-01.png)", "url(i/shop-directory-02.png)"];
		var bg_index = 0;
		$(this).click(function () { 
			bg_index++;
			if (bg_index == 2) {
				bg_index = 0;
			}
			var bg_img_src = bgs[bg_index];
			$(this).css("background-image", bg_img_src);
		});
	});
	// AJAX simulation for guide-shoreline-markup-tool.html
	$("#shoreline-markup-tool").each(function(index) {
		var bgs = ["i/guide-shoreline-markup-tool-01.png", "i/guide-shoreline-markup-tool-02.png", "i/guide-shoreline-markup-tool-03.png", "i/guide-shoreline-markup-tool-04.png"];
		var bg_index = 0;
		$(this).click(function () { 
			bg_index++;
			if (bg_index == 4) {
				bg_index = 0;
			}
			var bg_img_src = bgs[bg_index];
      		$(this).attr("src", bg_img_src);
		});
	});
	// AJAX simulation for guide-wind-direction-chart.html
	$("#wind-direction-chart").hover(
	function () {
		$(this).attr("src", "i/wind-directions-popup-02.png");
	}, 
	function () {
		$(this).attr("src", "i/wind-directions-popup-01.png");
	});
});