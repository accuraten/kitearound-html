(function() {
  $.fn.clicktoggle = function(a, b) {
    return this.each(function() {
      var clicked;
      clicked = false;
      $(this).click(function() {
        if (clicked) {
          clicked = false;
          return b.apply(this, arguments);
        }
        clicked = true;
        return a.apply(this, arguments);
      });
    });
  };

  $(document).ready(function() {
    var ka_styles, map_markers;
    $.fn.animateRotate = function(startAngle, endAngle, duration, easing, complete) {
      return this.each(function() {
        var elem;
        elem = $(this);
        $({
          deg: startAngle
        }).animate({
          deg: endAngle
        }, {
          duration: duration,
          easing: easing,
          step: function(now) {
            elem.css({
              "-moz-transform": "rotate(" + now + "deg)",
              "-webkit-transform": "rotate(" + now + "deg)",
              "-o-transform": "rotate(" + now + "deg)",
              "-ms-transform": "rotate(" + now + "deg)",
              transform: "rotate(" + now + "deg)"
            });
          },
          complete: complete || $.noop
        });
      });
    };
    $("a#button-to-top").click(function(event) {
      $("html,body").animate({
        scrollTop: 0
      }, 300);
    });
    $("#menu").collapse({
      toggle: false
    });
    $(".menu-button a").clicktoggle(function() {
      $("#menu").collapse('show');
      $(".menu-button i").animateRotate(0, -90, 300, "swing");
    }, function() {
      $("#menu").collapse('hide');
      $(".menu-button i").animateRotate(-90, 0, 300, "swing");
    });
    enquire.register("(max-width: 640px)", {
      match: function() {
        $(".wiki-content h2").each(function(index) {
          if ($(this).nextAll("h2").length > 0) {
            return $(this).nextUntil("h2").wrapAll("<div class='collapsible-h2-wrapper' />");
          } else {
            return $(this).nextAll().wrapAll("<div class='collapsible-h2-wrapper' />");
          }
        });
        $(".collapsible-h2-wrapper").each(function(index) {
          $(this).addClass("collapse-media");
          $(this).prev("h2").addClass("collapsed");
          return $(this).prev("h2").children().wrapAll("<a href='#uncollapse' class='collapsible-h2-wrapper-a' />");
        });
        return $(".collapsible-h2-wrapper-a").click(function() {
          $(event.target).closest("h2").toggleClass("collapsed").nextAll(".collapsible-h2-wrapper:lt(1)").toggleClass("collapse-media");
        });
      },
      unmatch: function() {
        $('a.collapsible-h2-wrapper-a').contents().unwrap();
        $("h2.collapsed").removeClass("collapsed");
        return $(".collapsible-h2-wrapper").removeClass("collapse-media").contents().unwrap();
      }
    });
    map_markers = [
      {
        lat: 10.916746485747343,
        lng: 108.2947011522217,
        name: "15kn",
        type: "weather-station",
        href: "spot.html"
      }, {
        lat: 10.940848856476558,
        lng: 108.19454473876954,
        name: "Willy Willy",
        type: "school",
        href: "school.html"
      }, {
        lat: 10.94691627753898,
        lng: 108.20003790283204,
        name: "Africa",
        type: "shop",
        href: "shop.html"
      }, {
        lat: 10.952815039985326,
        lng: 108.21325582885743,
        name: "Ozone",
        type: "shop",
        href: "shop.html"
      }
    ];
    ka_styles = [
      {
        featureType: "administrative",
        elementType: "all",
        stylers: [
          {
            color: "#00d3ff"
          }
        ]
      }, {
        featureType: "administrative",
        elementType: "geometry.stroke",
        stylers: [
          {
            color: "#00bee6"
          }
        ]
      }, {
        featureType: "administrative",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#005466"
          }
        ]
      }, {
        featureType: "administrative",
        elementType: "labels.text.stroke",
        stylers: [
          {
            color: "#00bee6"
          }
        ]
      }, {
        featureType: "landscape",
        elementType: "all",
        stylers: [
          {
            color: "#00a9cc"
          }
        ]
      }, {
        featureType: "poi",
        elementType: "all",
        stylers: [
          {
            visibility: "off"
          }
        ]
      }, {
        featureType: "road",
        elementType: "all",
        stylers: [
          {
            color: "#0095b3"
          }
        ]
      }, {
        featureType: "road",
        elementType: "labels",
        stylers: [
          {
            visibility: "off"
          }
        ]
      }, {
        featureType: "transit",
        elementType: "all",
        stylers: [
          {
            visibility: "simplified"
          }, {
            color: "#0095b3"
          }
        ]
      }, {
        featureType: "water",
        elementType: "all",
        stylers: [
          {
            color: "#0088a3"
          }, {
            visibility: "on"
          }
        ]
      }
    ];
    $("#spot-location-map").each(function(index) {
      var id, initialize, lat, lng, map, mapOptions, marker, marker_display, myLatLng, scrollwheel, showmarkers, zoom;
      lat = parseFloat($(this).attr("data-lat"));
      lng = parseFloat($(this).attr("data-lng"));
      scrollwheel = $(this).attr("data-scrollwheel") === 'true';
      showmarkers = $(this).attr("data-showmarkers") === 'true';
      marker_display = $(this).attr("data-marker-display") !== 'false';
      if ($(this).attr("data-zoom") == null) {
        zoom = 10;
      } else {
        zoom = parseInt($(this).attr("data-zoom"));
      }
      initialize = function() {};
      mapOptions = {
        center: {
          lat: lat,
          lng: lng
        },
        styles: ka_styles,
        panControl: false,
        zoomControl: true,
        scaleControl: true,
        scrollwheel: scrollwheel,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        mapTypeControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.LARGE,
          position: google.maps.ControlPosition.LEFT_BOTTOM
        },
        zoom: zoom
      };
      id = $(this).attr("id");
      map = new google.maps.Map(document.getElementById(id), mapOptions);
      map.setOptions;
      if (!showmarkers) {
        if (marker_display) {
          myLatLng = new google.maps.LatLng(mapOptions.center.lat, mapOptions.center.lng);
          marker = new MarkerWithLabel({
            position: myLatLng,
            icon: " ",
            map: map,
            labelContent: "<span class='fa-stack fa-2x'><i class='fa fa-circle fa-stack-2x marker-school'></i><i class='fa fa-map-marker fa-stack-1x fa-inverse'></i></span>",
            labelAnchor: new google.maps.Point(20, 20),
            labelClass: "map-marker-label2"
          });
          marker.setMap(map);
        }
      } else {
        jQuery.each(map_markers, function(i, item) {
          var css_marker_style;
          myLatLng = new google.maps.LatLng(item.lat, item.lng);
          css_marker_style = "marker-" + item.type;
          marker = new MarkerWithLabel({
            position: myLatLng,
            icon: " ",
            map: map,
            labelContent: "<div class='map-marker-container'><a class='map-marker' href='" + item.href + "'><div class='map-marker-icon'><span class='fa-stack fa-2x'><i class='fa fa-circle fa-stack-2x " + css_marker_style + "'></i><i class='fa fa-map-marker fa-stack-1x fa-inverse'></i></span></div><div class='map-marker-label'>" + item.name + "</div></a></div>",
            labelAnchor: new google.maps.Point(20, 10),
            labelClass: "map-marker-label2"
          });
          google.maps.event.addListener(marker, "click", function() {
            return window.location.href = item.href;
          });
          return marker.setMap(map);
        });
      }
      return;
      return google.maps.event.addDomListener(window, "load", initialize);
    });
    $('.share-widget').fixedsticky();
    $('.carousel').carousel();
    $('.selectpicker').selectpicker({
      style: 'btn-select'
    });
    $('.selectpicker-table').selectpicker({
      style: 'btn-select-table'
    });
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
      return $('.selectpicker, .selectpicker-table').selectpicker('mobile');
    }
  });

}).call(this);
